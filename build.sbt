name := "CS441OptHW3"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "com.novocode" % "junit-interface" % "0.8" % "test->default"
testOptions += Tests.Argument(TestFrameworks.JUnit, "-v")