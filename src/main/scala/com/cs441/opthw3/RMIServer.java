package com.cs441.opthw3;

import java.rmi.Naming;
import java.rmi.RemoteException;

//Program for server application

public class RMIServer extends RMIInterfaceImpl {
    public RMIServer() throws RemoteException {}
    public static void main(String[] args) {
        try {
            //Create an object of the interface implementation class
            RMIInterfaceImpl objectServer = new RMIInterfaceImpl();

            //Bind the remote object by the name 'aliasServer'
            Naming.rebind("aliasServer", objectServer);

            System.out.println("Server is ready");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
