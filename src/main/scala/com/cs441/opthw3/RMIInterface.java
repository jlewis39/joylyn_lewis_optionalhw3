package com.cs441.opthw3;

import java.rmi.Remote;
import java.rmi.RemoteException;

//Define the remote interface which extends the interface java.rmi.Remote and declare a remote method testRefIntegrity() which
//must declare java.rmi.RemoteException in its throws clause

public interface RMIInterface extends Remote {
    //Declare the method prototype
    boolean testRefIntegrity(DemoClass object1, DemoClass object2) throws RemoteException;
}
