package com.cs441.opthw3;

import java.io.Serializable;

//Create a demo class with attribute 'x'. Two references to the same object of this class will be passed from one JVM to another JVM in parameters in a single remote method call
//to demonstrate that the Java RMI framework holds the referential integrity property.

public class DemoClass implements Serializable {
    int x;
    //Constructor for DemoClass
    DemoClass(int y){
        this.x = y;
    }
}
