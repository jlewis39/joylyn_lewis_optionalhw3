package com.cs441.opthw3;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

//Implement the remote interface

public class RMIInterfaceImpl extends UnicastRemoteObject implements RMIInterface {
    //Default constructor to throw RemoteException from its parent constructor
    public RMIInterfaceImpl() throws RemoteException {
        super();
    }
    //Implementation of the function testRefIntegrity()
    public boolean testRefIntegrity(DemoClass object1, DemoClass object2){

        //Output initial values of the two objects passed from client to server
        System.out.println("Initial Values passed from Client to Server:");
        System.out.println("Object1.x = " + object1.x);
        System.out.println("Object2.x = " + object2.x);

        //Update attribute value of one of the objects
        object1.x = 200;

        //Output values of the two objects passed from client to server
        //after update done to one object attribute on server
        System.out.println("Values after change done to one object at Server:");
        System.out.println("Object1.x = " + object1.x);
        System.out.println("Object2.x = " + object2.x);

        //Return boolean result of comparison of the attribute values of the two objects
        if(object1.x == object2.x){
            return true;
        } else {
            return false;
        }
    }

}
