package com.cs441.opthw3;

import java.rmi.Naming;

//Program for client application
public class RMIClient {
    private RMIClient(){}
    public static void main(String[] args) {
        try {

            //Lookup method to find reference of remote object
            RMIInterface obj = (RMIInterface) Naming.lookup("aliasServer");

            //Create an object of DemoClass with attribute value 100
            DemoClass object = new DemoClass(100);

            //Create an alias for the previously created object
            DemoClass objectAlias = object;

            // Call the remote method using the obtained object
            boolean result = obj.testRefIntegrity(object,objectAlias);

            //Output message if test of referential integrity of the JAVA RMI framework holds
            if (result){
                System.out.println("Demo shows that two references to the same object passed from one JVM to another JVM in parameters in a single remote method call shows that  " +
                        "those references will refer to a single copy of the object in the receiving JVM. Thus, Java RMI framework holds the referential integrity property");
            }

        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
