package com.cs441.opthw3;

//Test cases for testing whether Java RMI framework holds the referential integrity property
import org.junit.Test;
import java.rmi.Naming;
import static org.junit.Assert.assertEquals;

public class RMIJunitTest {
    @Test
    //TestCase 1: Check that the result of passing two references to the same object passed from one JVM to another JVM in parameters in a single remote
    // method call actually refer to a single copy of the object in the receiving JVM
    public void testCase1() {
        try {

            //Lookup method to find reference of remote object
            RMIInterface obj = (RMIInterface) Naming.lookup("aliasServer");

            //Create an object of DemoClass with attribute value 100
            DemoClass object1 = new DemoClass(100);

            //Create an alias for the previously created object
            DemoClass object1Alias = object1;

            //Call the remote method using the obtained object
            boolean result = obj.testRefIntegrity(object1,object1Alias);

            //For testing referential integrity criteria, check whether expected value is 'true'
            assertEquals(true, result);

        } catch(Exception e) {
            System.err.println("Test exception: " + e.toString());
            e.printStackTrace();
        }

    }

    @Test
    //TestCase 2: Check that the result of passing two different objects passed from one JVM to another JVM in parameters in a single remote
    // method call refer to two different objects in the receiving JVM
    public void testCase2() {
        try {

            //Lookup method to find reference of remote object
            RMIInterface obj = (RMIInterface) Naming.lookup("aliasServer");

            //Create an object of DemoClass with attribute value 300
            DemoClass object1 = new DemoClass(300);

            //Create another object of DemoClass with attribute value 400
            DemoClass object2 = new DemoClass(400);

            // Call the remote method using the obtained object
            boolean result = obj.testRefIntegrity(object1,object2);

            //As two two objects are different, check whether the expected value is 'false'
            assertEquals(false, result);

        } catch(Exception e) {
            System.err.println("Test exception: " + e.toString());
            e.printStackTrace();
        }

    }

}
