### CS 441 Optional HW3 - Java or Scala RMI implementation that shows that the the Java RMI framework holds the referential integrity property
### Submitted By - Joylyn Lewis

### Details of important files included in the directory structure of the private repository:
- /src/main/scala/com/cs441/opthw3 folder contains the JAVA program files: **RMIInterface.java, DemoClass.java, RMIInterfaceImpl.java, RMIServer.java** and **RMIClient.java**
- /src/test/scala/com/cs441/opthw3 folder contains the file **RMIJunitTest.java** which conatins two test cases to test the referential integrity property
- /**build.sbt** provides dependency on library required for enabling writing Junit test cases

**NOTE:** In additon to the above files related to the code, file **'Output Results'.docx** provides screenshots of the program functionality and test outputs 

### How to run the code
- Import the project structure from the repository **https://bitbucket.org/jlewis39/joylyn_lewis_optionalhw3/src/master/** into IntelliJ
- If importing from IntelliJ -> you can clone it via VCS > Checkout from Version Control > Git. Use the URL highlighted above for the repository on bitbucket
- Go to the directory where all the Java class files are present and run javac *.java at commandline.
- To run the program using SBT
    - In command line, go to the root directory of the class files and run the command -> start rmiregistry
    - Go to the directory where all the class files are present and execute command -> sbt clean compile run
    - As there are multiple main methods, the below message will appear:  
         Multiple main classes detected, select one to run:  
         [1] com.cs441.opthw3.RMIClient  
         [2] com.cs441.opthw3.RMIServer  
    - Please enter number '2' at command line to run RMIServer.java program. A message 'Server is ready' will appear.
    - Open another instance of the command line and again go to the directory where all the class files are present and execute command -> sbt run
    - Again, the below message will appear:  
         Multiple main classes detected, select one to run:  
         [1] com.cs441.opthw3.RMIClient  
         [2] com.cs441.opthw3.RMIServer
    - Please enter number '1' at command line to run RMIClient.java program. The results of the remote call will be displayed on both the client and server consoles.  
- To test the functionality using SBT
    - Go to the project directory and execute the command 'sbt test'.
    - The number of tests run and passed will appear on the console.
    
####ASSUMPTIONS: As the homework description did not specify the need to use Logging or Configuration libraries, these two libraries have not been used in this homework. However, all other aspects of the homework including tests,sbt clean, compile, test, run, comments in the code have been taken care of. Please find file **'Output Results'.docx** in the repository which provides screenshots of the program functionality and test outputs 
    
    
    
    


